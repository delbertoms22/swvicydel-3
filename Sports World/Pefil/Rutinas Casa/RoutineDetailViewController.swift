//
//  RoutineDetailViewController.swift
//  SW_etapa_IV
//
//  Created by Mario Canto on 7/30/18.
//  Copyright © 2018 Aldo Gutierrez Montoya. All rights reserved.
//

import UIKit

final class RoutineDetailViewController: UIViewController {

    @IBOutlet weak var routineImageView: UIImageView!
    @IBOutlet weak var routineTitleLabel: UILabel!
    @IBOutlet weak var reproductionsLabel: UILabel!
    @IBOutlet var starButtons: [UIButton] = []
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var trainerNameLabel: UILabel!
    @IBOutlet weak var trainerImageView: UIImageView!
    @IBOutlet weak var trainerDescriptionLabel: UILabel!
    
    @IBAction func starWasPressed(_ sender: UIButton) {
        starButtons
            .forEach({ $0.isSelected = false })
        starButtons
            .filter({ $0.tag <= sender.tag })
            .forEach({ $0.isSelected = true })
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
