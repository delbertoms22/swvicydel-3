//
//  ViewController.swift
//  TableStyleNetflix
//
//  Created by VicktorManuel on 7/30/18.
//  Copyright © 2018 VicktorManuel. All rights reserved.
//

import UIKit

class RutinasCasaViewController: UIViewController,MoviesTableViewCellDelegate {
    func videoSelect(video: Video) {
        print(video.titulo)
        self.videoSeleccionado = video
        performSegue(withIdentifier: "segueVideoDescription", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueVideoDescription"{
            // let changeName = segue.destination as! DescripcionViewController
            //changeName.videoImagen = videoSeleccionado?.urlImagen
            //changeName.videoUrl = videoSeleccionado?.urlVideo
        }
    }
    @IBOutlet weak var tableView: UITableView!
    //Colleccion de videos
    var collecciones:[VideoCollection]! = []
    //Video seleccionado
    var videoSeleccionado:Video?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        for j in 0...10{
            
            var videos:[Video]! = []
            for i in 0...10{
                //Agrego videos
                let urlImagen = "http://www.digitalmarketinghq.com/wp-content/uploads/2013/02/video_icon.png"
                let urlVideo = "https://www.youtube.com/watch?v=RaUTTV0_dLY"
                videos.append(Video(titulo: "Mi video \(i)", urlImagen: urlImagen, urlVideo: urlVideo))
            }
            //Agrego la colleccion con los videos
            collecciones.append(VideoCollection(video: videos, titulo: "Nuevos \(j)"))
            
           
            tableView.reloadData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RutinasCasaViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collecciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MoviesTableViewCell
        cell.delegate = self
        let collection = collecciones[indexPath.row] as VideoCollection
        cell.titleCell.text = collection.titulo
        cell.collectionVideos = collection.video
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 154
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        APIManager.sharedInstance.getRutinasCasa(onSuccess: {response in
            DispatchQueue.main.async {
                
                //self.nivelLabel.text = String(APIManager.sharedInstance.nivel)
                //self.programaLabel.text = APIManager.sharedInstance.rutina
                // /} else {
                
                //}
                
                
            }
            
        })
        
    }
    
}

