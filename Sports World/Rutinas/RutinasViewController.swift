//
//  RutinasViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/12/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import PlayerKit
import AVKit
import MediaPlayer
var claveFinal: String!
class RutinasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
   
     var days = [Int]()
    var daysDateFormat = [Date]()
    var currentSelected = Date()
    let formatter = DateFormatter()
    var currentSegment: Int!
    private let cell = "RutinasCell"
    var thereIsCellTapped = false
    var selectedRowIndex = -1
    private let collectionCell = "RutinasCollectionViewCell"
    
    
    //MARK:- CONECTA LOS OUTLETS
    
    @IBOutlet weak var colleView: UICollectionView!
    @IBOutlet weak var series: UILabel!
    @IBOutlet weak var repeticiones: UILabel!
    @IBOutlet weak var descanso: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inbodyContainer: UIView!
    @IBOutlet weak var avancesContainer: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var instructionsView: UIView!
    var nameOfDays: [String] = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB","DOM"]
    var entrenamiento : [Entrenamiento] = [Entrenamiento]()
    var celdasVideo : [RutinasCell] = [RutinasCell]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        celdasVideo.removeDuplicates()
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "icon_agendar"), style: .done, target: self, action: #selector(RutinasViewController.gotoReservaInbody))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        formatter.dateFormat = "dd"
        let result = formatter.string(from: currentSelected)
        print("date", result)
        tabView.delegate = self
        tabView.dataSource = self
        
        
        segmentedControl.selectedSegmentIndex = 1


       
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 23)
         titleLabel.lineBreakMode = .byWordWrapping
          titleLabel.numberOfLines = 1
        let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //attributes for the second part of the string
        
        //initializing the attributed string and appending the two parts together
        let attrString = NSMutableAttributedString(string: SavedData.getTheName(), attributes: firstAttr)
        //setting the attributed string as an attributed text
        titleLabel.attributedText = attrString
        
        //finding the bounds of the attributed text and resizing the label accordingly
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
        titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
        
        //setting the label as the title view of the navigation bar
        navigationItem.titleView = titleLabel
        
        avancesContainer.isHidden = true
        inbodyContainer.isHidden = true
        //Customiza la navigatonBar
       
 
   //CONFIGURA LAS VISTAS.
        instructionsView.layer.borderColor = UIColor.gray.cgColor
        instructionsView.layer.borderWidth = 0.5
        self.tabView.rowHeight = 100
        

        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        self.repeticiones.text = ""
        self.descanso.text = ""
        self.series.text = ""
        
        
        
        self.getCalendarRoutines()
        
        
    }

    func getCalendarRoutines() {
        APIManager.sharedInstance.getCalendarRoutines( onSuccess: {response in
            
            DispatchQueue.main.async {
                if(response.code == 200){
                    let cal = Calendar.current
                    var date = cal.startOfDay(for: Date())
                    
                    self.currentSelected = date
                    var move = 0
                    let currentCalendar = NSCalendar.current
                    let timeUnitDay = NSCalendar.Unit.day
                    let daysBetween = currentCalendar.dateComponents([.day], from: cal.date(byAdding: .day, value: 0, to: response.fechaInicio)!, to: response.fechaFin).day!
                    
                    self.daysDateFormat = [Date]()
                    self.days = [Int]()
                    date = cal.date(byAdding: .day, value: 0, to: response.fechaInicio)!
                    for i in 0 ... daysBetween {
                        let day = cal.component(.day, from: date)
                        self.days.append(day)
                        self.daysDateFormat.append(date)
                        if(self.currentSelected == date){
                            move = i
                        }
                        date = cal.date(byAdding: .day, value: +1, to: date)!
                        
                    }
                    self.colleView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        let indexPath : IndexPath = IndexPath(row: move, section: 0)
                        
                        self.colleView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    })
                    
                    /*let dateFormatter = DateFormatter()
                     dateFormatter.dateFormat = "yyyy-MM-dd"
                     APIManager.sharedInstance.getEntrenamientos(date: dateFormatter.string(from: self.currentSelected), onSuccess: {response in
                     DispatchQueue.main.async {
                     self.entrenamiento = response.data
                     
                     self.tabView.reloadData()
                     }
                     
                     })*/
                    //self.entrenamiento = [Entrenamiento]()
                    //self.celdasVideo = [RutinasCell]()
                    self.tabView.reloadData()
                    self.updateCalendar()
                    
                }else{
                    Alert.ShowAlert(title: "", message: response.message, titleForTheAction: "Aceptar", in: self)
                }
            }
            
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //segmentedControl.selectedSegmentIndex = currentSegment
        
 let editButton   = UIBarButtonItem.init(image: UIImage(named: "icon_agendar"), style: .done, target: self, action: #selector(RutinasViewController.gotoReservaInbody))
        
        self.navigationItem.rightBarButtonItem = editButton
        
    }
   

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.celdasVideo.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        return self.celdasVideo[indexPath.row]
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.celdasVideo[indexPath.row]
        if cell.isSelectedCell {
            return 305
        }
        
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = self.celdasVideo[indexPath.row]
        if(!cell.isSelectedCell){
            cell.videoView.loadHTMLString(cell.entranamiento.video.replacingOccurrences(of: "640", with: "\(Int(self.view.bounds.width * 3))").replacingOccurrences(of: "360", with: "\(Int(600))"), baseURL: nil)
            
            self.repeticiones.text = "\(cell.entranamiento.repeticiones)"
            self.descanso.text = "\(cell.entranamiento.descanso)"
            self.series.text = "\(cell.entranamiento.series)"
            
            cell.isSelectedCell = true
        }else{
            cell.isSelectedCell = false
        }
        for cellTemp in self.celdasVideo{
            if(cellTemp != cell){
                cellTemp.videoView.loadHTMLString("", baseURL: nil)
                cellTemp.isSelectedCell = false
            }
        }
        
        
        
        
        //let screenSiz: CGRect = UIScreen.main.bounds
        
        
        
        
        
        // avoid paint the cell is the index is outside the bounds
        /*if self.selectedRowIndex != -1 {
            
        }
        
        if selectedRowIndex != indexPath.row {
            self.thereIsCellTapped = true
            if screenSiz == CGRect(x: 0.0, y: 0.0, width: 375.0, height: 667.0) {
                
                print("puta screen size", screenSiz)

            }
            self.selectedRowIndex = indexPath.row
        }
        else {
            // there is no cell selected anymore
            self.thereIsCellTapped = false
            self.selectedRowIndex = -1
        }*/
        //player.play()
        self.tabView.beginUpdates()
        self.tabView.endUpdates()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //segmentedControl.selectedSegmentIndex = currentSegment
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return  self.days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cal = Calendar.current
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCell, for: indexPath as IndexPath) as! RutinasCollectionViewCell
        cel.dayNumberLabel.text = String(days[indexPath.row])
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_MX") as Locale!
        dateFormatter.dateFormat = "EEE"
        cel.dayLabel.text =  dateFormatter.string(from: cal.date(byAdding: .day, value:0, to: self.daysDateFormat[indexPath.row])!)
        
        if(self.currentSelected.compare(self.daysDateFormat[indexPath.row]) == ComparisonResult.orderedSame ){
            
            cel.dayNumberLabel.layer.borderWidth = 2.0
            cel.dayNumberLabel.layer.borderColor = UIColor.white.cgColor
            cel.dayNumberLabel.layer.cornerRadius = cel.dayNumberLabel.bounds.width / 2
            
        }else{
            //cel.dayNumberLabel.textColor = UIColor.white
            
            cel.dayNumberLabel.layer.borderWidth = 0.0
            cel.dayNumberLabel.layer.borderColor = UIColor.clear.cgColor
            cel.dayNumberLabel.layer.cornerRadius = 0
        }
        return cel 
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentSelected = self.daysDateFormat[indexPath.row];
        for i in 0...(self.colleView.visibleCells.count - 1){
            let tempCell : RutinasCollectionViewCell = self.colleView.visibleCells[i] as! RutinasCollectionViewCell
            
            
            //tempCell.dayNumberLabel.textColor = UIColor.white
            
            tempCell.dayNumberLabel.layer.borderWidth = 0.0
            tempCell.dayNumberLabel.layer.borderColor = UIColor.clear.cgColor
            tempCell.dayNumberLabel.layer.cornerRadius = 0
            
        }
        let tempCellSelect : RutinasCollectionViewCell = self.colleView.cellForItem(at: indexPath) as! RutinasCollectionViewCell
        //tempCellSelect.dayNumberLabel.textColor = UIColor.red
        tempCellSelect.dayNumberLabel.layer.borderWidth = 2.0
        tempCellSelect.dayNumberLabel.layer.borderColor = UIColor.white.cgColor
        tempCellSelect.dayNumberLabel.layer.cornerRadius = tempCellSelect.dayNumberLabel.bounds.width / 2
        self.colleView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        
        self.updateCalendar()
        
        
        
    }
    func updateCalendar(){
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        APIManager.sharedInstance.getEntrenamientos(date: dateFormatter.string(from: self.currentSelected)) {response in
            

            DispatchQueue.main.async {
                self.celdasVideo.removeAll()
                self.entrenamiento.removeAll()
                self.entrenamiento = response.data
                
                var i = 0
                
                //self.entrenamiento = [Entrenamiento]()
                //self.celdasVideo = [RutinasCell]()
                
                for entranamientoTemp in self.entrenamiento {
                    let cel : RutinasCell = self.tabView.dequeueReusableCell(withIdentifier: self.cell) as! RutinasCell
                    cel.tag = i
                    /*self.repeticiones.text = String(entranamiento.repeticiones)
                    self.descanso.text = String(entranamiento.descanso)
                    self.series.text = String(entranamiento.series)*/
                    //claveFinal = entranamiento.clave
                    cel.exerciseName.text = entranamientoTemp.nombre
                    //cel.checkButton.tag = i
                    //cel.buttonState = entranamiento.completado
                    cel.repetitionsLabel.text = String("\(entranamientoTemp.repeticiones) repeticiones")
                    cel.videoView.scrollView.isScrollEnabled = true
                    cel.videoView.scrollView.bounces = false
                    cel.videoView.allowsInlineMediaPlayback = true
                    cel.videoView.mediaPlaybackRequiresUserAction = true
                    cel.entranamiento = entranamientoTemp
                    cel.checkButton.backgroundColor = UIColor.black
                    cel.checkButton.setImage(nil, for: .normal)
                    if entranamientoTemp.completado == true {
                        cel.checkButton.setImage(UIImage(named: "check_button")?.withRenderingMode(.alwaysOriginal), for: .normal)
                    } else {
                        cel.checkButton.backgroundColor = UIColor.black
                        
                    }
                    
                    
                    //cel.parent = self
                    self.celdasVideo.append(cel)
                   i = i + 1
                }
                
                
                /*let tempVideos = self.celdasVideo
                var tempList = [RutinasCell]()
                for temp in tempVideos{
                    
                    
                    if(tempList.filter({$0.entranamiento.orden == temp.entranamiento.orden}).count == 0){
                        tempList.append(temp)
                    }else if(tempList.count == 0){
                        tempList.append(temp)
                    }
                }
                self.celdasVideo = tempList*/
                self.tabView.reloadData()
            }
            
        }
    }
    
    
/////////////////////ACCIONES DE LOS BOTONES///////////////////////
    @IBAction func clickSegmented(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            avancesContainer.isHidden = true
            inbodyContainer.isHidden = false
            tabView.isHidden = true
            let titleLabel = UILabel()
            titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 23)
            titleLabel.lineBreakMode = .byWordWrapping
            titleLabel.numberOfLines = 1
            let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
            //attributes for the second part of the string
            
            //initializing the attributed string and appending the two parts together
            let attrString = NSMutableAttributedString(string: "Wellness test", attributes: firstAttr)
            //setting the attributed string as an attributed text
            titleLabel.attributedText = attrString
            
            //finding the bounds of the attributed text and resizing the label accordingly
            let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
            titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
            
            //setting the label as the title view of the navigation bar
            navigationItem.titleView = titleLabel
            instructionsView.isHidden = true
            colleView.isHidden = true
            currentSegment = sender.selectedSegmentIndex
            
        } else if sender.selectedSegmentIndex == 1 {
            
            avancesContainer.isHidden = true
            inbodyContainer.isHidden = true
            tabView.isHidden = true
            let titleLabel = UILabel()
            titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 23)
            titleLabel.lineBreakMode = .byWordWrapping
            titleLabel.numberOfLines = 1
            let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
            //attributes for the second part of the string
            
            //initializing the attributed string and appending the two parts together
            let attrString = NSMutableAttributedString(string: SavedData.getTheName(), attributes: firstAttr)
            //setting the attributed string as an attributed text
            titleLabel.attributedText = attrString
            
            //finding the bounds of the attributed text and resizing the label accordingly
            let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
            titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
            
            //setting the label as the title view of the navigation bar
            navigationItem.titleView = titleLabel
            tabView.isHidden = false
            instructionsView.isHidden = false
            colleView.isHidden = false
            currentSegment = sender.selectedSegmentIndex
        } else if sender.selectedSegmentIndex == 2 {
            segmentedControl.isHidden = false 
            colleView.isHidden = true
            avancesContainer.isHidden = false
            tabView.isHidden = true
            let titleLabel = UILabel()
            titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 23)
            titleLabel.lineBreakMode = .byWordWrapping
            titleLabel.numberOfLines = 1
            let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
            //attributes for the second part of the string
            
            //initializing the attributed string and appending the two parts together
            let attrString = NSMutableAttributedString(string: "Wellness test", attributes: firstAttr)
            //setting the attributed string as an attributed text
            titleLabel.attributedText = attrString
            
            //finding the bounds of the attributed text and resizing the label accordingly
            let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
            titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
            
            //setting the label as the title view of the navigation bar
            navigationItem.titleView = titleLabel
            inbodyContainer.isHidden = true 
            tabView.isHidden = true
            instructionsView.isHidden = true
           currentSegment = sender.selectedSegmentIndex
            
            
        }
    }


    @objc func gotoReservaInbody()  {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "CalendarioInBodyViewController")
        self.navigationController!.pushViewController(VC1, animated: true)
    }
    
}
