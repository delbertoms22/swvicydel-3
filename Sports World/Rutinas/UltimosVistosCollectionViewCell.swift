//
//  UltimosVistosCollectionViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/12/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class UltimosVistosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ultimosVistos: UIImageView!
    
}
