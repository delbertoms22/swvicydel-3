//
//  NuevosCollectionViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/12/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class NuevosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ultimosVistosImage: UIImageView!
    
}
