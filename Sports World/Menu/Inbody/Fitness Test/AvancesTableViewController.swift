//
//  AvancesTableViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 7/10/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import Charts
import SwiftyJSON
class AvancesTableViewController: UITableViewController {
    @IBOutlet weak var pesoChart: LineChartView!
    @IBOutlet weak var estaturaChart: UIView!
    @IBOutlet weak var mesesView: UIView!
    
    @IBOutlet weak var mineralesChart: UIView!
    
    @IBOutlet weak var actChart: UIView!
    
    var lineChartEntry = [ChartDataEntry]()
   
    @IBOutlet weak var rcc: UIView!
    @IBOutlet weak var imcChart: UIView!
    
    @IBOutlet weak var pgcChart: UIView!
    
    @IBOutlet weak var mgcChart: UIView!
    @IBOutlet weak var mmeChart: UIView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEstaturaChart()
        self.setMineralesChart()
        self.setPeso()
        self.setActChart()
        self.setMgcChart()
        self.setRccChart()
        self.setMmeChart()
        self.setPgcChart()
        self.setImcChart()
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 23)
        //attributes for the first part of the string
        
        let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //attributes for the second part of the string
        
        //initializing the attributed string and appending the two parts together
        let attrString = NSMutableAttributedString(string: "WELLNESS TEST", attributes: firstAttr)
        //setting the attributed string as an attributed text
        titleLabel.attributedText = attrString
        
        //finding the bounds of the attributed text and resizing the label accordingly
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
        titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
        
        //setting the label as the title view of the navigation bar
        navigationItem.titleView = titleLabel
        
        var a : CGFloat = 0
        var xValue = self.mesesView.bounds.width / CGFloat(APIManager.sharedInstance.mes.count)
        for month in APIManager.sharedInstance.mes{
            let label : UILabel = UILabel(frame: CGRect(x: xValue * a, y: 0, width: xValue, height: 20))
            label.textColor = UIColor.white
            label.font = UIFont(name: "LarkeNeue-Regular", size: 9)
            label.contentMode = .center
            label.text = month
            
            self.mesesView.addSubview(label)
            a = a + 1.0
        }
       
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        
    }
    var countTemp : Int = 0
    func addLine(fromPoint start: CGPoint, toPoint: CGPoint,viewGraph : UIView){
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: toPoint)
        line.path = linePath.cgPath
        line.strokeColor = UIColor.red.cgColor
        line.lineWidth = 2
        line.lineJoin = kCALineJoinRound
        viewGraph.layer.addSublayer(line)
        
        var dotPath = UIBezierPath(ovalIn: CGRect(x: start.x - 2.5, y: start.y - 2.5, width: 5, height: 5 ))
        
        var layer = CAShapeLayer()
        layer.path = dotPath.cgPath
        layer.strokeColor = UIColor.red.cgColor
        layer.backgroundColor = UIColor.red.cgColor
        layer.lineWidth = 5
        viewGraph.layer.addSublayer(layer)
        
        dotPath = UIBezierPath(ovalIn: CGRect(x: toPoint.x - 2.5, y: toPoint.y - 2.5, width: 5, height: 5 ))
        
        layer = CAShapeLayer()
        layer.path = dotPath.cgPath
        layer.lineWidth = 5
        layer.backgroundColor = UIColor.red.cgColor
        layer.strokeColor = UIColor.red.cgColor
        
        viewGraph.layer.addSublayer(layer)
        
        countTemp = countTemp + 1
        print(countTemp)
    }
    func setPeso() {
        var months = APIManager.sharedInstance.mes
        let numbers = APIManager.sharedInstance.pesoInbody2
        var inbodyValues = ["peso"]
        if(numbers.count > 0){
        for i in 0..<(numbers.count - 1) {
            let value = ChartDataEntry(x: Double(i), y: numbers[i])
            lineChartEntry.append(value)
            addLine(fromPoint: CGPoint(x: ((pesoChart.bounds.width / CGFloat( numbers.count)) * CGFloat(i)), y: ((CGFloat(numbers[i]) / CGFloat(numbers.max()! * 1.10) * pesoChart.bounds.height ))), toPoint: CGPoint(x: ((pesoChart.bounds.width / CGFloat( numbers.count)) * CGFloat(i + 1)), y: ((CGFloat(numbers[i + 1]) / CGFloat(numbers.max()! * 1.10) * pesoChart.bounds.height ))), viewGraph: pesoChart)
        }
        }
        
        /*let line1 = LineChartDataSet(values: lineChartEntry, label: "")
        line1.colors = [NSUIColor.red]
        let data = LineChartData()
        data.addDataSet(line1)
        pesoChart.data = data
        
        pesoChart.xAxis.labelTextColor = UIColor.white
        pesoChart.leftAxis.labelTextColor = UIColor.white
        pesoChart.xAxis.labelPosition = XAxis.LabelPosition.top
        pesoChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        pesoChart.leftAxis.axisMaximum = 100.0
        pesoChart.xAxis.labelTextColor = UIColor.white
        pesoChart.leftAxis.labelPosition = YAxis.LabelPosition.outsideChart
        pesoChart.leftAxis.xOffset = 20
        pesoChart.leftAxis.labelCount = inbodyValues.count
        pesoChart.leftAxis.forceLabelsEnabled = true
        
        pesoChart.rightAxis.forceLabelsEnabled = true
        pesoChart.xAxis.granularityEnabled = true
        pesoChart.xAxis.granularity = 1.0
        pesoChart.leftAxis.drawGridLinesEnabled = false
        pesoChart.rightAxis.drawGridLinesEnabled = false
        pesoChart.xAxis.drawGridLinesEnabled = false
        pesoChart.xAxis.labelTextColor = UIColor.white
        //lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        pesoChart.leftAxis.valueFormatter = IndexAxisValueFormatter(values:inbodyValues)
        pesoChart.leftAxis.labelTextColor = UIColor.white*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }

    func setEstaturaChart() {
        var monthsEstatura = APIManager.sharedInstance.mes
        let numbersEstatura = APIManager.sharedInstance.estaturaInbody2
        var inbodyValuesEstatura = ["estatura"]
        if(numbersEstatura.count > 0){
        for i in 0..<(numbersEstatura.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((estaturaChart.bounds.width / CGFloat( numbersEstatura.count)) * CGFloat(i)), y: ((CGFloat(numbersEstatura[i]) / CGFloat(numbersEstatura.max()! * 1.10) * estaturaChart.bounds.height ))), toPoint: CGPoint(x: ((estaturaChart.bounds.width / CGFloat( numbersEstatura.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersEstatura[i + 1]) / CGFloat(numbersEstatura.max()! * 1.10) * estaturaChart.bounds.height ))), viewGraph: estaturaChart)
        }
        }
        /*let line1 = LineChartDataSet(values: lineChartEntry, label: "")
        line1.colors = [NSUIColor.red]
        let data = LineChartData()
        data.addDataSet(line1)
        estaturaChart.data = data
        
        estaturaChart.xAxis.labelTextColor = UIColor.white
        estaturaChart.leftAxis.labelTextColor = UIColor.white
        estaturaChart.xAxis.labelPosition = XAxis.LabelPosition.top
        //estaturaChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: monthsEstatura)
        
        estaturaChart.xAxis.labelTextColor = UIColor.white
        estaturaChart.leftAxis.labelPosition = YAxis.LabelPosition.outsideChart
        estaturaChart.leftAxis.xOffset = 20
        estaturaChart.leftAxis.labelCount = inbodyValuesEstatura.count
        estaturaChart.leftAxis.forceLabelsEnabled = true
        
        estaturaChart.rightAxis.forceLabelsEnabled = true
        estaturaChart.xAxis.granularityEnabled = true
        estaturaChart.xAxis.granularity = 1.0
        estaturaChart.leftAxis.drawGridLinesEnabled = false
        estaturaChart.rightAxis.drawGridLinesEnabled = false
        estaturaChart.xAxis.drawGridLinesEnabled = false
        estaturaChart.xAxis.labelTextColor = UIColor.clear
        //lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        estaturaChart.leftAxis.valueFormatter = IndexAxisValueFormatter(values:inbodyValuesEstatura)
        estaturaChart.leftAxis.labelTextColor = UIColor.white*/
    }
    func setMineralesChart() {
        var monthsMinerales = APIManager.sharedInstance.mes
        let numbersMinerales = APIManager.sharedInstance.mineralesInbody2
        var inbodyValuesMinerales = ["minerales"]
        
        if(numbersMinerales.count > 0){
        for i in 0..<(numbersMinerales.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((mineralesChart.bounds.width / CGFloat( numbersMinerales.count)) * CGFloat(i)), y: ((CGFloat(numbersMinerales[i]) / CGFloat(numbersMinerales.max()! * 1.10) * mineralesChart.bounds.height ))), toPoint: CGPoint(x: ((mineralesChart.bounds.width / CGFloat( numbersMinerales.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersMinerales[i + 1]) / CGFloat(numbersMinerales.max()! * 1.10) * mineralesChart.bounds.height ))), viewGraph: mineralesChart)
        }
        }
        
        /*
        let line1 = LineChartDataSet(values: lineChartEntry, label: "")
        line1.colors = [NSUIColor.red]
        let data = LineChartData()
        data.addDataSet(line1)
        mineralesChart.data = data
        
        mineralesChart.xAxis.labelTextColor = UIColor.white
        mineralesChart.leftAxis.labelTextColor = UIColor.white
        mineralesChart.xAxis.labelPosition = XAxis.LabelPosition.top
        mineralesChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: monthsMinerales)
        
        mineralesChart.xAxis.labelTextColor = UIColor.white
        mineralesChart.leftAxis.labelPosition = YAxis.LabelPosition.outsideChart
        mineralesChart.leftAxis.xOffset = 20
        mineralesChart.leftAxis.labelCount = inbodyValuesMinerales.count
        mineralesChart.leftAxis.forceLabelsEnabled = true
        
        mineralesChart.rightAxis.forceLabelsEnabled = true
        mineralesChart.xAxis.granularityEnabled = true
        mineralesChart.xAxis.granularity = 1.0
        mineralesChart.leftAxis.drawGridLinesEnabled = false
        mineralesChart.rightAxis.drawGridLinesEnabled = false
        mineralesChart.xAxis.drawGridLinesEnabled = false
        mineralesChart.xAxis.labelTextColor = UIColor.clear
        //lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        mineralesChart.leftAxis.valueFormatter = IndexAxisValueFormatter(values: inbodyValuesMinerales)
        mineralesChart.leftAxis.labelTextColor = UIColor.white*/
    }
    
    func setActChart() {
        var monthsAct = APIManager.sharedInstance.mes
        let numbersAct = APIManager.sharedInstance.mineralesInbody2
        var inbodyValuesAct = ["act"]
      if(numbersAct.count > 0){
        for i in 0..<(numbersAct.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((actChart.bounds.width / CGFloat( numbersAct.count)) * CGFloat(i)), y: ((CGFloat(numbersAct[i]) / CGFloat(numbersAct.max()! * 1.10) * actChart.bounds.height ))), toPoint: CGPoint(x: ((actChart.bounds.width / CGFloat( numbersAct.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersAct[i + 1]) / CGFloat(numbersAct.max()! * 1.10) * actChart.bounds.height ))), viewGraph: actChart)
        }
        }
        
       
    }
    func setMgcChart () {
        var monthsMgc = APIManager.sharedInstance.mes
        let numbersMgc = APIManager.sharedInstance.mcgInbody2
        var inbodyValuesMgc = ["mgc"]
        if(numbersMgc.count > 0){
        for i in 0..<(numbersMgc.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((mgcChart.bounds.width / CGFloat( numbersMgc.count)) * CGFloat(i)), y: ((CGFloat(numbersMgc[i]) / CGFloat(numbersMgc.max()! * 1.10) * mgcChart.bounds.height ))), toPoint: CGPoint(x: ((mgcChart.bounds.width / CGFloat( numbersMgc.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersMgc[i + 1]) / CGFloat(numbersMgc.max()! * 1.10) * mgcChart.bounds.height ))), viewGraph: mgcChart)
        }
        }
    }
    func setRccChart () {
        var monthsMgc = APIManager.sharedInstance.mes
        let numbersRcc = APIManager.sharedInstance.rccInbody2
        var inbodyValuesMgc = ["mgc"]
        if(numbersRcc.count > 0){
        for i in 0..<(numbersRcc.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((rcc.bounds.width / CGFloat( numbersRcc.count)) * CGFloat(i)), y: ((CGFloat(numbersRcc[i]) / CGFloat(numbersRcc.max()! * 1.10) * rcc.bounds.height ))), toPoint: CGPoint(x: ((rcc.bounds.width / CGFloat( numbersRcc.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersRcc[i + 1]) / CGFloat(numbersRcc.max()! * 1.10) * rcc.bounds.height ))), viewGraph: rcc)
        }
        }
    }
    func setMmeChart() {
        var monthsMgc = APIManager.sharedInstance.mes
        let numbersMme = APIManager.sharedInstance.mmeInbody2
        var inbodyValuesMgc = ["mgc"]
        if(numbersMme.count > 0){
        for i in 0..<(numbersMme.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((mmeChart.bounds.width / CGFloat( numbersMme.count)) * CGFloat(i)), y: ((CGFloat(numbersMme[i]) / CGFloat(numbersMme.max()! * 1.10) * mmeChart.bounds.height ))), toPoint: CGPoint(x: ((mmeChart.bounds.width / CGFloat( numbersMme.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersMme[i + 1]) / CGFloat(numbersMme.max()! * 1.10) * mmeChart.bounds.height ))), viewGraph: mmeChart)
        }
        }
    }
    
    func setPgcChart() {
        var monthsMgc = APIManager.sharedInstance.mes
        let numbersPgc = APIManager.sharedInstance.pgcInbody2
        var inbodyValuesMgc = ["mgc"]
        if(numbersPgc.count > 0){
        for i in 0..<(numbersPgc.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((pgcChart.bounds.width / CGFloat( numbersPgc.count)) * CGFloat(i)), y: ((CGFloat(numbersPgc[i]) / CGFloat(numbersPgc.max()! * 1.10) * pgcChart.bounds.height ))), toPoint: CGPoint(x: ((pgcChart.bounds.width / CGFloat( numbersPgc.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersPgc[i + 1]) / CGFloat(numbersPgc.max()! * 1.10) * pgcChart.bounds.height ))), viewGraph: pgcChart)
        }
        }
    }
    func setImcChart() {
        var monthsMgc = APIManager.sharedInstance.mes
        let numbersImc = APIManager.sharedInstance.imcInbody2
        var inbodyValuesMgc = ["mgc"]
        if(numbersImc.count > 0){
        for i in 0..<(numbersImc.count - 1) {
            
            addLine(fromPoint: CGPoint(x: ((imcChart.bounds.width / CGFloat( numbersImc.count)) * CGFloat(i)), y: ((CGFloat(numbersImc[i]) / CGFloat(numbersImc.max()! * 1.10) * imcChart.bounds.height ))), toPoint: CGPoint(x: ((imcChart.bounds.width / CGFloat( numbersImc.count)) * CGFloat(i + 1)), y: ((CGFloat(numbersImc[i + 1]) / CGFloat(numbersImc.max()! * 1.10) * imcChart.bounds.height ))), viewGraph: imcChart)
        }
        }
    }
    
}
