//
//  InbodyTableViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/8/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import Charts
class InbodyTableViewCell: UITableViewCell {
    @IBOutlet weak var proteinas: UILabel!
    @IBOutlet weak var minerales: UILabel!
    @IBOutlet weak var aguaCorporalTotal: UILabel!
    @IBOutlet weak var masaGrasaCorporal: UILabel!
    @IBOutlet weak var masaMusculoEsqueleto: UILabel!
    @IBOutlet weak var indiceMasaCorporal: UILabel!
    @IBOutlet weak var porcentajeGrasaCorporal: UILabel!
    @IBOutlet weak var relacionCinturaCadera: UILabel!
    @IBOutlet weak var barChart: BarChartView!
    
    
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
                proteinas.addBottomBorderWithColor(proteinas, color: UIColor.white, width: 0.5)
        minerales.addBottomBorderWithColor(minerales, color: UIColor.white, width: 0.5)
        aguaCorporalTotal.addBottomBorderWithColor(aguaCorporalTotal, color: UIColor.white, width: 0.5)
        masaGrasaCorporal.addBottomBorderWithColor(masaGrasaCorporal, color: UIColor.white, width: 0.5)
        masaMusculoEsqueleto.addBottomBorderWithColor(masaMusculoEsqueleto, color: UIColor.white, width: 0.5)
        indiceMasaCorporal.addBottomBorderWithColor(indiceMasaCorporal, color: UIColor.white, width: 0.5)
        porcentajeGrasaCorporal.addBottomBorderWithColor(porcentajeGrasaCorporal, color: UIColor.white, width: 0.5)
        relacionCinturaCadera.addBottomBorderWithColor(relacionCinturaCadera, color: UIColor.white, width: 0.5)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
