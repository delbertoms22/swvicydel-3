//
//  DetalleRecetaViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/25/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class DetalleRecetaViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var recetaImage: UIImageView!
    @IBOutlet weak var descripcionReceta: UITextView!
    
    @IBOutlet weak var backView: UIView!
    var recetaNombre = ""
    var recetaDescripcion = ""
    var recetaImagen = ""
    var selectedReceta : Receta = Receta()
    override func viewDidLoad() {
        super.viewDidLoad()
        backView.layer.cornerRadius = 8
        descripcionReceta.delegate = self
       // nombreReceta.text = recetaNombre
       // descripcionReceta.text = recetaDescripcion
        let titleLabel = UILabel()
        titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 20)
        //attributes for the first part of the string
        
        let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //attributes for the second part of the string
        
        //initializing the attributed string and appending the two parts together
        let attrString = NSMutableAttributedString(string: self.selectedReceta.nombre, attributes: firstAttr)
        //setting the attributed string as an attributed text
        titleLabel.attributedText = attrString
        
        //finding the bounds of the attributed text and resizing the label accordingly
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
        titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
        
        //setting the label as the title view of the navigation bar
        navigationItem.titleView = titleLabel
     //   recetaImage.downloadImage(downloadURL: recetaImagen, completion: { result in
            
       // })
        
         recetaNombre  = self.selectedReceta.nombre
        descripcionReceta.text = self.selectedReceta.descripcion
        recetaImage.downloadImage(downloadURL: self.selectedReceta.foto, completion: { result in
             })
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        dismissKeyboard()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        descripcionReceta.setContentOffset(CGPoint.zero, animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
