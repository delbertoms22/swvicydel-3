//
//  ClaseCategoriasTableViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 8/2/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

protocol OtherController : NSObjectProtocol {
    func loadNewScreen(controller: UIViewController) -> Void;
}
var descriptionHtml: String = ""

class ClaseCategoriasTableViewCell: UITableViewCell {
   
    weak var delegate: OtherController?


    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var entrenadorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func showClaseDetail(_ sender: Any) {
      
        APIManager.sharedInstance.getClassDescription(idClass: ffff! , onSuccess: { result in
            
            DispatchQueue.main .async {
            
               
                if result.status == true {
                     var vc = ClaseCategoriasDetailViewController()
                    if((self.delegate?.responds(to: Selector(("loadNewScreen:")))) != nil)
                    {
                        self.delegate?.loadNewScreen(controller: vc)
                    }
                
                } else {
                    Alert.ShowAlert(title: "", message: result.message, titleForTheAction: "Aceptar", in: ClaseCategoriasViewController())
                }
            
            }
        }, onFailure: { error in
            
        })
    }
    }
    
    

